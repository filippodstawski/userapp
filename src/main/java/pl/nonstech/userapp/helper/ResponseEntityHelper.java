package pl.nonstech.userapp.helper;

import org.springframework.http.ResponseEntity;

public class ResponseEntityHelper {

    public static <E> ResponseEntity<E> generateResponseEntity(final E body) {
        if (body instanceof Boolean && !((Boolean) body)) {
            return ResponseEntity.notFound().build();
        }
        if (body == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(body);
    }

}
