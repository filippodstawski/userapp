package pl.nonstech.userapp.repository;

import org.springframework.stereotype.Component;
import pl.nonstech.userapp.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserRepository {

    final static ArrayList<User> users = new ArrayList<>();

    public List<User> findAllUsers() {
        return users;
    }

    public User findUserById(final Long userId) {
        return users.stream()
                .filter(user -> user.getId().equals(userId))
                .findFirst()
                .orElse(null);
    }

    public List<User> findUsersByLastName(final String lastName) {
        final List<User> usersWithLastName = users.stream()
                .filter(user -> lastName.equals(user.getLastName()))
                .collect(Collectors.toCollection(ArrayList::new));
        if (usersWithLastName.size() > 0) {
            return usersWithLastName;
        }
        return null;
    }

    public User saveUser(final User user) {
        user.setId(Long.valueOf(users.size() + 1));
        users.add(user);
        return user;
    }

    public User updateUser(final User userUpdateData, final Long userId) {
        final User user = findUserById(userId);
        if (user == null) {
            return null;
        }
        if (userUpdateData.getName() != null) {
            user.setName(userUpdateData.getName());
        }
        if (userUpdateData.getLastName() != null) {
            user.setLastName(userUpdateData.getLastName());
        }
        if (userUpdateData.isActiveInvoice() != null) {
            user.setActiveInvoice(userUpdateData.isActiveInvoice());
        }
        return user;
    }

    public Boolean deleteUser(final Long userId) {
        final User userToDelete = findUserById(userId);
        if (userToDelete == null) {
            return false;
        }
        users.removeIf(user -> userId.equals(user.getId()));
        return true;
    }

}
