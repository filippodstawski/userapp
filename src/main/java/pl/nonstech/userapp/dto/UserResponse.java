package pl.nonstech.userapp.dto;

public class UserResponse {

    Long id;
    String name;
    String lastName;
    Boolean activeInvoice;

    public Long getId() {
        return this.id;
    }

    public void setId(Long value) {
        this.id = value;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String value) {
        this.lastName = value;
    }

    public Boolean isActiveInvoice() {
        return this.activeInvoice;
    }

    public void setActiveInvoice(Boolean value) {
        this.activeInvoice = value;
    }

}
