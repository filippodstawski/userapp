package pl.nonstech.userapp.dto;

public class UserRequest {

    String name;
    String lastName;
    Boolean activeInvoice;

    public String getName() {
        return this.name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String value) {
        this.lastName = value;
    }

    public Boolean isActiveInvoice() {
        return this.activeInvoice;
    }

    public void setActiveInvoice(Boolean value) {
        this.activeInvoice = value;
    }

}
