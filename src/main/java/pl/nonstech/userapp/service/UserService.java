package pl.nonstech.userapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.nonstech.userapp.model.User;
import pl.nonstech.userapp.repository.UserRepository;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public List<User> findAllUsers() {
        return userRepository.findAllUsers();
    }

    public User findUserById(final Long userId) {
        return userRepository.findUserById(userId);
    }

    public List<User> findUsersByLastName(final String lastName) {
        return userRepository.findUsersByLastName(lastName);
    }

    public User addNewUser(final User user) {
        return userRepository.saveUser(user);
    }

    public User updateUser(final User user, final Long userId) {
        return userRepository.updateUser(user, userId);
    }

    public Boolean deleteUser(final Long userId) {
        return userRepository.deleteUser(userId);
    }

}
