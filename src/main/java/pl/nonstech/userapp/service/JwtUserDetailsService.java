package pl.nonstech.userapp.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    final static Map<String, String> jwtUsers = new HashMap<String, String>() {{
        put("test", "$2y$12$lrg81OxGyXHuPKogML2AxOx33rPrcCX.uwKz1/XeDczfdCPd9heC2");
    }};

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        if (jwtUsers.containsKey(username)) {
            return new User(username, jwtUsers.get(username),
                    new ArrayList<>());
        } else {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
    }

}