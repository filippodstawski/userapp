package pl.nonstech.userapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.nonstech.userapp.converter.UserRequestToUserConverter;
import pl.nonstech.userapp.converter.UserToUserResponseConverter;
import pl.nonstech.userapp.dto.UserRequest;
import pl.nonstech.userapp.dto.UserResponse;
import pl.nonstech.userapp.helper.ResponseEntityHelper;
import pl.nonstech.userapp.model.User;
import pl.nonstech.userapp.service.UserService;

import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserRequestToUserConverter userRequestToUserConverter;

    @Autowired
    private UserToUserResponseConverter userToUserResponseConverter;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/user", method = RequestMethod.GET)
    public ResponseEntity<List<UserResponse>> allUsers() {
        return ResponseEntityHelper.generateResponseEntity(userToUserResponseConverter.convertAll(userService.findAllUsers()));
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.GET)
    public ResponseEntity<UserResponse> userById(@PathVariable Long id) {
        final UserResponse userResponse = userToUserResponseConverter.convert(userService.findUserById(id));
        return ResponseEntityHelper.generateResponseEntity(userResponse);
    }

    @RequestMapping(value = "/user/", method = RequestMethod.GET)
    public ResponseEntity<List<UserResponse>> usersByLastName(@RequestParam String lastName) {
        final List<UserResponse> userResponse = userToUserResponseConverter.convertAll(userService.findUsersByLastName(lastName));
        return ResponseEntityHelper.generateResponseEntity(userResponse);
    }

    @RequestMapping(value = "/user/", method = RequestMethod.POST)
    public ResponseEntity<UserResponse> addNewUser(@RequestBody UserRequest userRequest) {
        final User user = userRequestToUserConverter.convert(userRequest);
        return ResponseEntityHelper.generateResponseEntity(userToUserResponseConverter.convert(userService.addNewUser(user)));
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.PUT)
    public ResponseEntity<UserResponse> update(@PathVariable Long id, @RequestBody UserRequest userRequest) {
        final User userUpdateData = userRequestToUserConverter.convert(userRequest);
        return ResponseEntityHelper.generateResponseEntity(userToUserResponseConverter.convert(userService.updateUser(userUpdateData, id)));
    }

    @RequestMapping(value = "/user/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deleteUser(@PathVariable Long id) {
        return ResponseEntityHelper.generateResponseEntity(userService.deleteUser(id));
    }

}