package pl.nonstech.userapp.converter;

import org.springframework.stereotype.Component;
import pl.nonstech.userapp.dto.UserResponse;
import pl.nonstech.userapp.model.User;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserToUserResponseConverter {

    public UserResponse convert(final User user){
        if (user == null) {
            return null;
        }
        final UserResponse userResponse = new UserResponse();
        userResponse.setId(user.getId());
        userResponse.setName(user.getName());
        userResponse.setLastName(user.getLastName());
        userResponse.setActiveInvoice(user.isActiveInvoice());
        return userResponse;
    }

    public List<UserResponse> convertAll(final List<User> users){
        if (users == null) {
            return null;
        }
        final List<UserResponse> userResponses = new ArrayList<>();
        users.forEach(user -> userResponses.add(convert(user)));
        return userResponses;
    }

}
