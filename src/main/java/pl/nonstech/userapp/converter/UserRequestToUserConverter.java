package pl.nonstech.userapp.converter;

import org.springframework.stereotype.Component;
import pl.nonstech.userapp.dto.UserRequest;
import pl.nonstech.userapp.model.User;

@Component
public class UserRequestToUserConverter {

    public User convert(final UserRequest userRequest) {
        if (userRequest == null) {
            return null;
        }
        final User user = new User();
        user.setName(userRequest.getName());
        user.setLastName(userRequest.getLastName());
        user.setActiveInvoice(userRequest.isActiveInvoice());
        return user;
    }

}
